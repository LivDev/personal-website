import React from 'react';
import PropTypes from 'prop-types';

import { Card } from 'semantic-ui-react';

class DetailedProject extends React.Component {
  constructor(props){
    super(props);

    this.handleCardClick = this.props.handleCardClick;
  }
  render(){
    let project = this.props.project;
    return(
      <Card onClick={this.handleCardClick}>
        <Card.Content>
          <Card.Header>{project.title}</Card.Header>
          <Card.Meta>{project.time_period}</Card.Meta>
          <Card.Description>
            <div>{project.description}</div>
            <div>{project.branch}</div>
          </Card.Description>
        </Card.Content>
      </Card>
    );
  }
}

DetailedProject.propTypes = {
  handleCardClick: PropTypes.func,
  project: PropTypes.object
};

export default DetailedProject;