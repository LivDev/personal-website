import React from 'react';
import PropTypes from 'prop-types';

import { Card } from 'semantic-ui-react';

class SimpleProject extends React.Component {
  constructor(props){
    super(props);

    this.handleCardClick = this.props.handleCardClick;
  }

  render(){
    let project = this.props.project;
    return(
      <Card onClick={this.handleCardClick}>
        <Card.Content>
          <Card.Header>{project.title}</Card.Header>
          <Card.Meta>{project.time_period}</Card.Meta>
          <Card.Description>{project.description}</Card.Description>
        </Card.Content>
      </Card>
    );
  }
}

SimpleProject.propTypes = {
  handleCardClick: PropTypes.func,
  project: PropTypes.object
};

export default SimpleProject;