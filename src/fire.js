import firebase from 'firebase';

var config = { /* COPY THE ACTUAL CONFIG FROM FIREBASE CONSOLE */
  apiKey: 'AIzaSyB0BsJMQMqfSrIdRfXpfeiDch_mAjhbvvk',
  authDomain: 'personalsite-550f7.firebaseapp.com',
  databaseURL: 'https://personalsite-550f7.firebaseio.com',
  projectId: 'personalsite-550f7',
  storageBucket: 'personalsite-550f7.appspot.com',
  messagingSenderId: '1080661544537'
};
var fire = firebase.initializeApp(config);

export default fire;