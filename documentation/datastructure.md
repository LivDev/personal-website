# Datastructure documentation

## Datastructure Skills

Icons for Skills:
https://www.flaticon.com/search?word=programmer&license=selection

Skills are stored in JSON in an ES6 class.

Format:

```json
[
  {
    "name": "skillname",
    "rating": "rating as string"
  }
]
```

Rating:

|Rating |Weight         |
|-------|---------------|
|0 - 3  | Beginner      |
|4 - 6  | Intermediate  |
|7 - 10 | Expert        |

## Datastructure Projectlist

Projektlist are stored in JSON in an ES6 class.

Format:

```json
[
  {
    "title": "Title of the Project",
    "time_perion": "time period as String with - as devider",
    "company_name": "name of the company",
    "description": "short project description",
    "role": "rolle in the Project",
    "branch": "branch of the project",
    "tasks": ["array of tasks during this project"],
    "systems_and_methods": ["array of used systems and methods"]
  }
]
```
